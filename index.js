const MIME_MAP = new Map()
  .set('js', 'text/javascript')
  .set('txt', 'text/plain')
  .set('ico', 'image/x-icon')
  .set('css', 'text/css')
  .set('html', 'text/html')
  .set('gif', 'image/gif')
  .set('json', 'application/json');

const 
  fs = require('fs'),
  { S3Client, ListObjectsV2Command, DeleteObjectsCommand, PutObjectCommand } = require('@aws-sdk/client-s3');

module.exports = function(options) {
  const client = new S3Client(options);

  this.listObjects = async function(bucket) {
    const command = new ListObjectsV2Command({
      Bucket: bucket
    });
    const response = await client.send(command);
    return response.Contents;
  };

  this.deleteObjects = async function(bucket, keys) {
    const objects = keys.map(k => {
      return {
        Key: k
      };
    });
    const command = new DeleteObjectsCommand({
      Bucket: bucket,
      Delete: {
        Objects: objects
      }
    });
    await client.send(command);
  };

  this.uploadFile = async function(path, bucket, key, mimeType, isPublic) {
    const filename = path.substring(path.lastIndexOf('/') + 1);
    const mime = mimeType || MIME_MAP.get(filename.substring(filename.lastIndexOf('.') + 1));
    const file = await fs.promises.readFile(path);
    key = key || filename;
    await upload(file, bucket, key, mime, isPublic);
  };
  this.uploadBody = async function(body, bucket, key, mimeType, isPublic) {
    await upload(body, bucket, key, mimeType, isPublic);
  }
  async function upload(body, bucket, key, mimeType, isPublic) {
    let options = {
      Body: body,
      Key: key,
      Bucket: bucket,
      ContentType: mimeType
    };
    if (isPublic) {
      options.ACL = 'public-read';
    }
    const command = new PutObjectCommand(options);
    await client.send(command);
  }
};